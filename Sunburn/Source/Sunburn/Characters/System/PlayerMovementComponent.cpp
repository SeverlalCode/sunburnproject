// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerMovementComponent.h"


// Sets default values for this component's properties
UPlayerMovementComponent::UPlayerMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}



// Called when the game starts
void UPlayerMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	

	// ...
	
}


float UPlayerMovementComponent::Module(FVector vector)
{
	return FMath::Sqrt((vector.X*vector.X) + (vector.Y*vector.Y) + (vector.Z*vector.Z));
}

float UPlayerMovementComponent::CalculateDirection(FVector actorlocation)
{
	FVector origin = FVector(0, 0, 0);

	origin.X += 200.0f;
	origin.Y = actorlocation.Y;
	origin.Z = actorlocation.Z;
	actorlocation.Y += 200.0f;
	float OA = ((origin.X*actorlocation.X) + (origin.Y*actorlocation.Y) + (origin.Z*actorlocation.Z));

	return (asin(OA / ((Module(origin))*(Module(actorlocation)))));
	 
}

void UPlayerMovementComponent::SetVelocity(TEnumAsByte<ELocomotionState> Behaviour)
{

	MaxVelocity = MovementSpeed.FindRef(Behaviour);
			
}

float UPlayerMovementComponent::GetAxisValue(float axis, TEnumAsByte<EAxisType> FWDorOther)
{

	float NewAxis = 1.0f;

	if (bIsGhostMovement)
	{
		switch (FWDorOther)
		{
		case(Fwd):
			//AxisForForwardMovement
			switch (GhostMovement.Forward)
			{
			case(slow):
				NewAxis = NewAxis * (MaxVelocity / 100.0f);
				break;
			case(medium):
				NewAxis = NewAxis * (MaxVelocity / 50.0f);
				break;
			case(fast):
				NewAxis = NewAxis * (MaxVelocity / 25.0f);
				break;
			}
			break;
		case(1):
			//AxisForBackwardMovement
			switch (GhostMovement.BackWard)
			{
			case(slow):
				NewAxis = NewAxis * (MaxVelocity / 100.0f);
				break;
			case(medium):
				NewAxis = NewAxis * (MaxVelocity / 50.0f);
				break;
			case(fast):
				NewAxis = NewAxis * (MaxVelocity / 25.0f);
				break;
			}
			break;
		case(2):
			//AxisForRightWardMovement
			switch (GhostMovement.Rightward)
			{
			case(0):
				NewAxis = NewAxis * (MaxVelocity / 100.0f);
				break;
			case(1):
				NewAxis = NewAxis * (MaxVelocity / 50.0f);
				break;
			case(2):
				NewAxis = NewAxis * (MaxVelocity / 25.0f);
				break;
			}
			break;
		case(3):
			//AxisForLeftWardMovement
			switch (GhostMovement.Leftward)
			{
			case(0):
				NewAxis = NewAxis * (MaxVelocity / 100.0f);
				break;
			case(1):
				NewAxis = NewAxis * (MaxVelocity / 50.0f);
				break;
			case(2):
				NewAxis = NewAxis * (MaxVelocity / 25.0f);
				break;
			}
			break;
		}
	}

	return NewAxis * axis;

}

FVector UPlayerMovementComponent::RotateVector(FVector RotatingVector, float Angle, ABaseCharacter* Target)
{
	
	return RotatingVector = Target->GetActorRotation().RotateVector(RotatingVector);

}

void UPlayerMovementComponent::UpdateMovement(ABaseCharacter* Sargeon, FVector RotatedVector)
{

	FVector NewLocation = Sargeon->GetActorLocation() + RotatedVector;
	Sargeon->SetActorLocation(NewLocation);
	UE_LOG(LogTemp, Warning, TEXT("Proceed to move"));

}

float UPlayerMovementComponent::GetAngleFromDirection(FVector location, ABaseCharacter* target)
{
	
	FVector xv = FVector (200,0,0);
	FVector dir = location - target->GetActorLocation();

	float dot = (xv.X*dir.X) + (xv.Y*dir.Y) + (xv.Z*dir.Z);

	float xvl = FMath::Sqrt((xv.X*xv.X) + (xv.Y*xv.Y) + (xv.Z*xv.Z));
	float dirl = FMath::Sqrt((dir.X*dir.X) + (dir.Y*dir.Y) + (dir.Z*dir.Z));

	float angle = ((FMath::Acos(dot / (xvl*dirl)))*180)/PI;

	if (location.Y - target->GetActorLocation().Y < 0)
	{
		return angle * -1;
	}
	else
		return angle;

}

bool UPlayerMovementComponent::TurnCharacterbyAngle(float Angle,ABaseCharacter* CharacterToRotate, float delta, float InSpeed)
{

	FRotator rotation = FRotator(0, 0, 0);
	rotation.Yaw = Angle;
	FRotator Interpolation = FMath::RInterpTo(CharacterToRotate->GetActorRotation(), rotation, delta, InSpeed);

	//if nearly equal to the interpolation with an error tollerance by 1
	if (CharacterToRotate->GetActorRotation().Yaw <= rotation.Yaw && CharacterToRotate->GetActorRotation().Yaw >= rotation.Yaw - 10)
		return true;
	else
	{
		CharacterToRotate->SetActorRotation(Interpolation);
		return false;
	}
	

}

//artifical intellgence
bool UPlayerMovementComponent::MoveCharacterToLocation(ABaseCharacter* target ,FVector Destination, float MaxDistanceTollerance)
{
	
	FVector dir = Destination - target->GetActorLocation();
	FVector normalizedDirection = dir / Module(dir);
	FVector newdir = normalizedDirection * 2.0f;
	FVector UpdatedVector = newdir + target->GetActorLocation();

	if (Module(dir) > MaxDistanceTollerance)
	{
		target->SetActorLocation(UpdatedVector);
		UE_LOG(LogTemp, Warning, TEXT("Distance: %f"), Module(dir));
		return false;
	}
	else
		return true;



}

void UPlayerMovementComponent::InitializeMovementSpeed()
{

	if (bIsGhostMovement)
	{
		MovementSpeed.Add(floating, 0.0f);
		MovementSpeed.Add(ghostwalking, 1000.0f);
	}
	else
	{
		MovementSpeed.Add(idle, 0.0f);
		MovementSpeed.Add(Walking, 150.f);
		MovementSpeed.Add(running, 375.0f);
	}
}

// Called every frame
void UPlayerMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	
}

