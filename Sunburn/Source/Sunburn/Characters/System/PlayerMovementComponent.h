// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BaseCharacter.h"
#include "PlayerMovementComponent.generated.h"


UENUM()
enum ELocomotionState
{
	idle,
	Walking,
	running,
	floating,
	ghostwalking
};

UENUM()
enum EMovementType
{
	slow,
	medium,
	fast
};


UENUM()
enum EAxisType
{
	Fwd,
	Bwd,
	Rwd,
	Lwd
};

USTRUCT(BlueprintType)
struct FMovement
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EMovementType> Forward;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EMovementType> BackWard;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EMovementType> Rightward;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EMovementType> Leftward;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SUNBURN_API UPlayerMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerMovementComponent();

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "Movement")
		bool bIsGhostMovement;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float CurrentVelocity;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MaxVelocity;
	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Movement")
		FVector Speed;
		FVector FWD_NEXT;// the predicted forward movement
		FVector RWD_NEXT;// the predicted rightward movement
	UPROPERTY(EditAnywhere, blueprintReadWrite,  Category = "Movement")
	TMap<TEnumAsByte<ELocomotionState>, float> MovementSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FMovement GhostMovement;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		float Quote;//a modifier for flying movement;

	UFUNCTION(blueprintCallable, Category = "Math")
		float Module(FVector vector);

	UFUNCTION(blueprintCallable, Category = "Movement")
		float CalculateDirection(FVector actorlocation);
	UFUNCTION(blueprintCallable, Category = "Movement")
		void SetVelocity(TEnumAsByte<ELocomotionState> Behaviour);
	UFUNCTION(blueprintCallable, Category = "Movement")
		float GetAxisValue(float axis, TEnumAsByte<EAxisType> FWDorOther);
	UFUNCTION(blueprintCallable, Category = "Movement")
		FVector RotateVector(FVector RotatingVector, float Angle, ABaseCharacter* Target);
	UFUNCTION(blueprintCallable, Category = "Movement")
		void UpdateMovement(ABaseCharacter* Sargeon, FVector RotatedVector);

	UFUNCTION(blueprintCallable, Category = "Artificial Intellgence")
		float GetAngleFromDirection(FVector location, ABaseCharacter* target);
	UFUNCTION(blueprintCallable, Category = "Artificial Intellgence")
		bool TurnCharacterbyAngle(float Angle, ABaseCharacter* CharacterToRotate, float delta, float Inspeed);
	UFUNCTION(blueprintCallable, Category = "Aritificial Intelligence")
		bool MoveCharacterToLocation(ABaseCharacter* target, FVector Destination, float MaxDistanceTollerance);

	UFUNCTION(blueprintCallable, Category = "Rules")
		void InitializeMovementSpeed();
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
