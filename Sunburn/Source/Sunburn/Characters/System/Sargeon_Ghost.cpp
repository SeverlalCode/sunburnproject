// Fill out your copyright notice in the Description page of Project Settings.

#include "Sargeon_Ghost.h"



ASargeon_Ghost::ASargeon_Ghost()
{
	//Enable the update method
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//Create Components
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	MainCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Main Camera"));
	MovementComponent = CreateDefaultSubobject<UPlayerMovementComponent>(TEXT("Player Movement Component"));
	USceneComponent* DirectionalArrow = CreateDefaultSubobject<USceneComponent>(TEXT("Forward Arrow"));

	//Set attachments
	MainCamera->SetupAttachment(RootComponent);
	MainCamera->SetRelativeLocation(FVector(-150.0f, 0.0f, 250.0f));
	MainCamera->SetRelativeRotation(FRotator(-45.0f, 0.0f, 0.0f));

	//Initial Setup
	AutoPossessPlayer = EAutoReceiveInput::Player0;
	AutoReceiveInput = EAutoReceiveInput::Player0;
	
	MovementComponent->bIsGhostMovement = true;
	MovementComponent->InitializeMovementSpeed();
	MovementComponent->SetVelocity(floating);
	MovementComponent->Quote = 5.0f;
	
	SoulFragments = 1;
	MaxTowerDistance = 2400.f;
	bCanMove = true;

	

}

void ASargeon_Ghost::BeginPlay()
{
	Super::BeginPlay();

	

}

void ASargeon_Ghost::Tick(float DeltaSeconds)
{

	Super::Tick(DeltaSeconds);


}

void ASargeon_Ghost::SetupPlayerInputComponent(UInputComponent * PlayerInputComponent)
{

	Super::SetupPlayerInputComponent(InputComponent);

	//Axis Binding
	InputComponent->BindAxis("Camera Movement", this, &ASargeon_Ghost::OnCameraMovement);
	InputComponent->BindAxis("Camera Look Up", this, &ASargeon_Ghost::OnCameraLookUp);
	InputComponent->BindAxis("Forward Movement", this, &ASargeon_Ghost::OnForwardMovement);
	InputComponent->BindAxis("Rightward Movement", this, &ASargeon_Ghost::OnRightwardMovement);
	InputComponent->BindAxis("Take Quote", this, &ASargeon_Ghost::OnTakingQuote);
	InputComponent->BindAxis("Less Quote", this, &ASargeon_Ghost::OnLessingQuote);
}

void ASargeon_Ghost::OnCameraMovement(float axis)
{
	
	FRotator rotation = GetActorRotation();
	rotation.Yaw += axis * 4.0f;

	SetActorRotation(rotation);

}

void ASargeon_Ghost::OnCameraLookUp(float axis)
{

	FRotator rotation = GetActorRotation();
	rotation.Pitch += axis*4.0f;

	SetActorRotation(rotation);

}

void ASargeon_Ghost::OnForwardMovement(float axis)
{
	if (Controller) {

		//Enable movement only and if player press the movement input key
		if (axis != 0.0f)
		{
			UE_LOG(LogTemp, Warning, TEXT("key is pressed"));

			MovementComponent->SetVelocity(ghostwalking);
			
			//Construct Variables
			FVector FWD_Movement = FVector(1.0f, 0.0f, 0.0f);
			FVector ActorLocation = this->GetActorLocation();
			float AxisAngle = this->GetActorRotation().Yaw;

			//Scale FWD Movement by Axis Value
			if(axis > 0.0f)
				axis = MovementComponent->GetAxisValue(axis,Fwd);
			else
				axis = MovementComponent->GetAxisValue(axis, Bwd);

			FWD_Movement = FWD_Movement * axis;

			//Rotate Vector;
			FVector RotatedVector = MovementComponent->RotateVector(FWD_Movement, AxisAngle, this);

			MovementComponent->FWD_NEXT = RotatedVector;

			CheckMovement_FWD();

			if(bCanMove)
				MovementComponent->UpdateMovement(this, RotatedVector);
			
		}
		else
		{
			MovementComponent->SetVelocity(floating);
			MovementComponent->FWD_NEXT = FVector(0, 0, 0);
		}
	}
}

void ASargeon_Ghost::OnRightwardMovement(float axis)
{

	if (Controller)
	{

		if (axis != 0)
		{
			MovementComponent->SetVelocity(ghostwalking);

			FVector RWD_Movement = FVector(0.0f, 1.0f, 0.0f);
			FVector ActorLocation = this->GetActorLocation();
			float AxisAngle = this->GetActorRotation().Yaw;

			//Scale FWD Movement by Axis Value
			if (axis > 0.0f)
				axis = MovementComponent->GetAxisValue(axis, Rwd);
			else
				axis = MovementComponent->GetAxisValue(axis, Lwd);

			RWD_Movement = RWD_Movement * axis;

			//Rotate Vector;
			FVector RotatedVector = MovementComponent->RotateVector(RWD_Movement, AxisAngle, this);

			MovementComponent->RWD_NEXT = RotatedVector;

			CheckMovement_RWD();

			if(bCanMove)
				MovementComponent->UpdateMovement(this, RotatedVector);
		}
		else
		{
			MovementComponent->SetVelocity(floating);
			MovementComponent->RWD_NEXT = FVector(0, 0, 0);
		}

	}

}

void ASargeon_Ghost::OnTakingQuote(float axis)
{

	if (Controller && axis != 0.0f)
	{
		FVector movement = FVector(0, 0, 1);
		movement.Z *= MovementComponent->Quote;
		FVector newloc = this->GetActorLocation() + movement;

		float distance = CalculateDistanceFromTower(newloc);

		if (distance < MaxTowerDistance)
			bCanMove = true;
		else
			bCanMove = false;

		if(bCanMove)
			this->SetActorLocation(newloc);
	}

}

void ASargeon_Ghost::OnLessingQuote(float axis)
{

	if (Controller && axis != 0.0f)
	{
		FVector movement = FVector(0, 0, 1);
		movement.Z = MovementComponent->Quote * axis;
		FVector newloc = this->GetActorLocation() - movement;

		if (newloc.Z > 0.0f)
			bCanMove = true;
		else
			bCanMove = false;

		if (bCanMove)
			this->SetActorLocation(newloc);

	}

}

void ASargeon_Ghost::CheckMovement_FWD()
{
#define unset FVector (0,0,0)

	if (ConnectedBlackTower != nullptr)
	{
		FVector prediction = MovementComponent->FWD_NEXT + this->GetActorLocation();
		float distance = CalculateDistanceFromTower(prediction);

		if (distance < MaxTowerDistance)
		{
			bCanMove = true;
		}
		else
			bCanMove = false;
	}

}

void ASargeon_Ghost::CheckMovement_RWD()
{

#define unset FVector (0,0,0)

	if (ConnectedBlackTower != nullptr)
	{
		FVector prediction = MovementComponent->RWD_NEXT + this->GetActorLocation();
		float distance = CalculateDistanceFromTower(prediction);

		if (distance < MaxTowerDistance)
		{
			bCanMove = true;
		}
		else
			bCanMove = false;
	}

}

void ASargeon_Ghost::IncreaseSoulFragments(int amount)
{

	SoulFragments += amount;

}

void ASargeon_Ghost::DecreaseSoulFragments(int amount)
{

	SoulFragments -= amount;

}

void ASargeon_Ghost::RegisterBlackTower(ABlackTower * NewBlackTower)
{

	ConnectedBlackTower = NewBlackTower;

}

void ASargeon_Ghost::UnregisterBlackTower()
{

	ConnectedBlackTower = nullptr;

}

ABlackTower * ASargeon_Ghost::FindNearestBlackTower()
{
#define invalid -1

	int TEMP_ID = invalid; // invalid ID
	float Distance = 0;
	float Prev_distance = 0;
	FVector dir = FVector(0, 0, 0);

	if (WorldBlackTowers.Num() > 1)//if we have enought tower for check distance
	{
		UE_LOG(LogTemp, Warning, TEXT("Ci sono abbastanza torri per controllare la distanza"));

		for (int i = 0; i <= WorldBlackTowers.Num(); i++)
		{

			if (i == 0)
			{
				dir = WorldBlackTowers[i]->GetActorLocation() - this->GetActorLocation();
				Distance = FMath::Sqrt((dir.X * dir.X) + (dir.Y*dir.Y) + (dir.Z * dir.Z));
				TEMP_ID = i;
			}
			else
			{
				Prev_distance = Distance;
				dir = WorldBlackTowers[i]->GetActorLocation() - this->GetActorLocation();
				Distance = FMath::Sqrt((dir.X * dir.X) + (dir.Y*dir.Y) + (dir.Z * dir.Z));

				if (Distance < Prev_distance)
					TEMP_ID = i;
				else
					TEMP_ID = i - 1;
			}
		}

		if (TEMP_ID > invalid)
		{
			UE_LOG(LogTemp, Error, TEXT("Qualcosa e' anato storto - non trovo nessuna torre valida"));
			return WorldBlackTowers[TEMP_ID];
		}
		else
			return nullptr;

	}
	if (WorldBlackTowers.Num() == 1)// we have only 1 tower
		return WorldBlackTowers[0];
	else
		return nullptr;

}

float ASargeon_Ghost::CalculateDistanceFromTower(FVector direction)
{

	return FMath::Sqrt((direction.X*direction.X) + (direction.Y*direction.Y) + (direction.Z*direction.Z));

}










