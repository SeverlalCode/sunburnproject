// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Characters/System/BaseCharacter.h"
#include "PlayerMovementComponent.h"
#include "Structures/BlackTower.h"
#include "Sargeon_Ghost.generated.h"

/**
 * 
 */
UCLASS()
class SUNBURN_API ASargeon_Ghost : public ABaseCharacter
{
	GENERATED_BODY()

public:
	ASargeon_Ghost();

	UPROPERTY(EditAnywhere, blueprintReadWrite, Category ="Components")
		UPlayerMovementComponent* MovementComponent;
	UPROPERTY(EditAnywhere, blueprintReadWrite, Category = "Componets")
		UCameraComponent* MainCamera;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
		int SoulFragments;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
		float DistanceFromTower;
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Info")
		ABlackTower* ConnectedBlackTower;
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Info")
		TArray<ABlackTower*> WorldBlackTowers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rules")
		float MaxTowerDistance;
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Rules")
		bool bCanMove;
	

public:

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


public:
	UFUNCTION(blueprintCallable, Category = "Movement")
		void OnCameraMovement(float axis);
	UFUNCTION(blueprintCallable, Category = "Movement")
		void OnCameraLookUp(float axis);
	UFUNCTION(blueprintCallable, Category = "Movement")
		void OnForwardMovement(float axis);
	UFUNCTION(blueprintCallable, Category = "Movement")
		void OnRightwardMovement(float axis);
	UFUNCTION(blueprintCallable, Category = "Movement")
		void OnTakingQuote(float axis);
	UFUNCTION(blueprintCallable, Category = "Movement")
		void OnLessingQuote(float axis);


	UFUNCTION()
		void CheckMovement_FWD();
	UFUNCTION()
		void CheckMovement_RWD();

	UFUNCTION(blueprintCallable, Category = "Gameplay")
		void IncreaseSoulFragments(int amount);
	UFUNCTION(blueprintCallable, Category = "Gameplay")
		void DecreaseSoulFragments(int amount);


	UFUNCTION(blueprintCallable, Category = "Tower")
		void RegisterBlackTower(ABlackTower* NewBlackTower);
	UFUNCTION(blueprintCallable, Category = "Tower")
		void UnregisterBlackTower();
	UFUNCTION(blueprintCallable, Category = "Tower")
		ABlackTower* FindNearestBlackTower();// compare towers distance and get the nearest tower world reference
	UFUNCTION(blueprintCallable, Category = "Tower")
		float CalculateDistanceFromTower(FVector direction);





};
