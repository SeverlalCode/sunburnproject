// Fill out your copyright notice in the Description page of Project Settings.

#include "Sargeon_Physic.h"


ASargeon_Physic::ASargeon_Physic()
{

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	AutoReceiveInput = EAutoReceiveInput::Player0;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Component"));
	SkeletalMesh = CreateDefaultSubobject <USkeletalMeshComponent>(TEXT("Sargeon Skeletal"));
	MovementComponent = CreateDefaultSubobject<UPlayerMovementComponent>(TEXT("Sargeon Movement Component"));

	CapsuleComponent->SetupAttachment(RootComponent);
	CapsuleComponent->SetCapsuleRadius(35);
	CapsuleComponent->SetCapsuleHalfHeight(100);
	CapsuleComponent->SetRelativeLocation(FVector(0, 0, 170));
	CapsuleComponent->SetRelativeRotation(FRotator(0, 0, 0));

	SkeletalMesh->SetupAttachment(CapsuleComponent);
	SkeletalMesh->SetRelativeLocation(FVector(0, 0, 0));
	SkeletalMesh->SetRelativeRotation(FRotator(0, 0, 0));

	MovementComponent->bIsGhostMovement = false;
	MovementComponent->InitializeMovementSpeed();


}
void ASargeon_Physic::BeginPlay()
{

	Super::BeginPlay();

}

void ASargeon_Physic::Tick(float DeltaSeconds)
{

	Super::Tick(DeltaSeconds);

}
