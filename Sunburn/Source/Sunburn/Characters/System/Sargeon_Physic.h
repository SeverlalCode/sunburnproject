// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Characters/System/BaseCharacter.h"
#include "PlayerMovementComponent.h"
#include "Sargeon_Physic.generated.h"

/**
 * 
 */
UCLASS()
class SUNBURN_API ASargeon_Physic : public ABaseCharacter
{
	GENERATED_BODY()

public:

	ASargeon_Physic();

	/*Components*/
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Components")
		USkeletalMeshComponent* SkeletalMesh;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Components")
		UCapsuleComponent* CapsuleComponent;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Components")
		UPlayerMovementComponent* MovementComponent;

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaSeconds)override;


	
};
