// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Structures/IteractableStructure.h"
#include "BlackTower.generated.h"

/**
 * 
 */
UCLASS()
class SUNBURN_API ABlackTower : public AIteractableStructure
{
	GENERATED_BODY()
	
};
