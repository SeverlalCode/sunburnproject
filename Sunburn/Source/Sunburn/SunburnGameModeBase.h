// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SunburnGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SUNBURN_API ASunburnGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
